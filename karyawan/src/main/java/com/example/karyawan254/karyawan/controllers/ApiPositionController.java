package com.example.karyawan254.karyawan.controllers;

import com.example.karyawan254.karyawan.models.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.karyawan254.karyawan.repositories.PositionRepo;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")

public class ApiPositionController {

    @Autowired
    private PositionRepo positionRepo;

    //  Menampilkan data
    @GetMapping("/position")
    public ResponseEntity<List<Position>> GetAllPosition(){
        try {
            List<Position> position = this.positionRepo.findAll();
            return new ResponseEntity<>(position, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //  Open form & Save data
    @PostMapping("/position")
    public ResponseEntity<Object> SavePosition(@RequestBody Position position){
        Position positionData = this.positionRepo.save(position);
        if (positionData.equals(position)){
            return new ResponseEntity<>("success",HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>("faild",HttpStatus.BAD_REQUEST);
        }
    }

    //  mengedit data(Edit)
    @GetMapping("/position/{id}")
    public ResponseEntity<List<Position>> GetPositionById(@PathVariable("id") Long id) {
        try{
            Optional<Position> position = this.positionRepo.findById(id);
            if (position.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(position, HttpStatus.OK);
                return rest;
            } else{
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchposition/{keyword}")
    public ResponseEntity<List<Position>> SearchPositionName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Position> position = this.positionRepo.SearchPosition(keyword);
            return new ResponseEntity<>(position, HttpStatus.OK);
        } else {
            List<Position> position = this.positionRepo.findAll();
            return new ResponseEntity<>(position, HttpStatus.OK);
        }
    }

    @GetMapping("/positionmapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<Position> position = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Position> pageTuts;

            pageTuts = positionRepo.findAll(pagingSort);

            position = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("position", position);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //  update data(Update)
    @PutMapping("/position/{id}")
    public ResponseEntity<Object> UpdatePosition(@RequestBody Position position, @PathVariable("id") Long id)
    {
        Optional<Position> categoryData = this.positionRepo.findById(id);

        if (categoryData.isPresent())
        {
            position.setId(id);
            this.positionRepo.save(position);

            ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
            return rest;
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

    //  delete data(Delete)
    @DeleteMapping("/position/{id}")
    public ResponseEntity<Object> DeletePosition(@PathVariable("id")Long id)
    {
        this.positionRepo.deleteById(id);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }
}
