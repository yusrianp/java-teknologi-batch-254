package com.example.karyawan254.karyawan.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="Cuti")
public class Cuti {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long Id;

    @Column(name = "cuti_jenis", nullable = false)
    private String CutiJenis;

    @Column(name = "cuti_mulai", nullable = false)
    private Date CutiMulai;

    @Column(name = "cuti_selesai", nullable = false)
    private Date CutiSelesai;

    @Column(name = "cuti_lama", nullable = false)
    private Long CutiLama;

    @Column(name = "cuti_alasan", nullable = false)
    private String CutiAlasan;

    @ManyToOne
    @JoinColumn(name="employee_id", insertable = false, updatable=false)
    public Employee employee;

    @Column(name="employee_id")
    private Long EmployeeId;


    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getCutiJenis() {
        return CutiJenis;
    }

    public void setCutiJenis(String cutiJenis) {
        CutiJenis = cutiJenis;
    }

    public Date getCutiMulai() {
        return CutiMulai;
    }

    public void setCutiMulai(Date cutiMulai) {
        CutiMulai = cutiMulai;
    }

    public Date getCutiSelesai() {
        return CutiSelesai;
    }

    public void setCutiSelesai(Date cutiSelesai) {
        CutiSelesai = cutiSelesai;
    }

    public Long getCutiLama() {
        return CutiLama;
    }

    public void setCutiLama(Long cutiLama) {
        CutiLama = cutiLama;
    }

    public String getCutiAlasan() {
        return CutiAlasan;
    }

    public void setCutiAlasan(String cutiAlasan) {
        CutiAlasan = cutiAlasan;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Long getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(Long employeeId) {
        EmployeeId = employeeId;
    }
}
