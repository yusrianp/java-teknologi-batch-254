package com.example.karyawan254.karyawan.controllers;

import com.example.karyawan254.karyawan.repositories.PositionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/position/")
public class PositionController {

    @GetMapping(value="index")
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("position/index");
        return view;
    }
}
