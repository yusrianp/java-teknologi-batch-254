package com.example.karyawan254.karyawan.repositories;

import com.example.karyawan254.karyawan.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role, Long> {

}
