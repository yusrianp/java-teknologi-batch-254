package com.example.karyawan254.karyawan.controllers;

import com.example.karyawan254.karyawan.models.Position;
import com.example.karyawan254.karyawan.repositories.CutiRepo;
import com.example.karyawan254.karyawan.models.Cuti;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiCutiController {

    @Autowired
    private CutiRepo cutiRepo;

    //  Menampilkan data
    @GetMapping("/cuti")
    public ResponseEntity<List<Cuti>> GetAllCuti(){
        try {
            List<Cuti> cuti = this.cutiRepo.findAll();
            return new ResponseEntity<>(cuti, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //  Save data
    @PostMapping("/cuti")
    public ResponseEntity<Object> SaveCuti(@RequestBody Cuti cuti)
    {
        try {
            this.cutiRepo.save(cuti);
            return new ResponseEntity<>(cuti, HttpStatus.CREATED);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }
    //  mengedit data(Edit)
    @GetMapping("/cuti/{id}")
    public ResponseEntity<List<Cuti>> GetCutiById(@PathVariable("id") Long id) {
        try{
            Optional<Cuti> cuti = this.cutiRepo.findById(id);
            if (cuti.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(cuti, HttpStatus.OK);
                return rest;
            } else{
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    //  update data(Update)
    @PutMapping("/cuti/{id}")
    public ResponseEntity<Object> UpdateCuti(@RequestBody Cuti cuti, @PathVariable("id") Long id)
    {
        Optional<Cuti> cutiData = this.cutiRepo.findById(id);

        if (cutiData.isPresent())
        {
            cuti.setId(id);
            this.cutiRepo.save(cuti);

            ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
            return rest;
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/searchcuti/{keyword}")
    public ResponseEntity<List<Cuti>> SearchCutiName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Cuti> cuti = this.cutiRepo.SearchCuti(keyword);
            return new ResponseEntity<>(cuti, HttpStatus.OK);
        } else {
            List<Cuti> cuti = this.cutiRepo.findAll();
            return new ResponseEntity<>(cuti, HttpStatus.OK);
        }
    }

    @GetMapping("/cutimapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<Cuti> cuti = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Cuti> pageTuts;

            pageTuts = cutiRepo.findAll(pagingSort);

            cuti = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("cuti", cuti);
            response.put("currentPage",pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/cuti/{id}")
    public ResponseEntity<Object> DeleteCuti(@PathVariable("id") Long id)
    {
        this.cutiRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.CREATED);
    }
}
