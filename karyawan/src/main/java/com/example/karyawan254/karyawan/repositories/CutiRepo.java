package com.example.karyawan254.karyawan.repositories;

import com.example.karyawan254.karyawan.models.Cuti;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CutiRepo extends JpaRepository<Cuti, Long> {
    @Query("FROM Cuti i Inner Join Employee e on i.EmployeeId = e.id WHERE lower(e.EmployeeName) LIKE lower(concat('%',?1,'%') ) ")
    List<Cuti> SearchCuti(String keyword);
}
