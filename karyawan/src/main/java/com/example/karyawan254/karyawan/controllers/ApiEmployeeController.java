package com.example.karyawan254.karyawan.controllers;

import com.example.karyawan254.karyawan.repositories.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.karyawan254.karyawan.models.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")

public class ApiEmployeeController {

    @Autowired
    private EmployeeRepo employeeRepo;

    //  Menampilkan data
    @GetMapping("/employee")
    public ResponseEntity<List<Employee>> GetAllEmployee(){
        try {
            List<Employee> employee = this.employeeRepo.findAll();
            return new ResponseEntity<>(employee, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //  Save data
    @PostMapping("/employee")
    public ResponseEntity<Object> SaveEmployee(@RequestBody Employee employee)
    {
        try {
            this.employeeRepo.save(employee);
            return new ResponseEntity<>(employee, HttpStatus.CREATED);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<List<Employee>> GetEmployeeById(@PathVariable("id") Long id)
    {
        try {
            Optional<Employee> employee = this.employeeRepo.findById(id);

            if (employee.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(employee, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/employee/{id}")
    public ResponseEntity<List<Employee>> UpdateEmployee(@PathVariable("id") Long id, @RequestBody Employee employee)
    {
        try {
            Optional<Employee> employeeData = this.employeeRepo.findById(id);

            if (employeeData.isPresent())
            {
                employee.setId(id);
                this.employeeRepo.save(employee);
                ResponseEntity rest = new ResponseEntity<>(employee, HttpStatus.CREATED);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/employee/{id}")
    public ResponseEntity<Object> DeleteEmployee(@PathVariable("id") Long id)
    {
        this.employeeRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.CREATED);
    }
}
