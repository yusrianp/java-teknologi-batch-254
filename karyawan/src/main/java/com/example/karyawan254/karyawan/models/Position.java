package com.example.karyawan254.karyawan.models;
import javax.persistence.*;
@Entity
@Table(name="Position")
public class Position {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long Id;

    @Column(name = "position_code")
    private String PositionCode;

    @Column(name = "position_name")
    private String PositionName;

    @Column(name="position_description")
    private String PositionDescription;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getPositionCode() {
        return PositionCode;
    }

    public void setPositionCode(String positionCode) {
        PositionCode = positionCode;
    }

    public String getPositionName() {
        return PositionName;
    }

    public void setPositionName(String positionName) {
        PositionName = positionName;
    }

    public String getPositionDescription() {
        return PositionDescription;
    }

    public void setPositionDescription(String positionDescription) {
        PositionDescription = positionDescription;
    }
}
