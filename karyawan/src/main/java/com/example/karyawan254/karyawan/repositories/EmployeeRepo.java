package com.example.karyawan254.karyawan.repositories;
import com.example.karyawan254.karyawan.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeRepo extends JpaRepository<Employee, Long> {
    @Query("FROM Employee WHERE PositionId = ?1")
    List<Employee> FindByPositionId(Long positionId);

    @Query("FROM Employee WHERE RoleId = ?1")
    List<Employee> FindByRoleId(Long roleId);
}
