package com.example.karyawan254.karyawan.controllers;

import com.example.karyawan254.karyawan.models.Position;
import com.example.karyawan254.karyawan.models.Role;
import com.example.karyawan254.karyawan.repositories.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiRoleController {
    @Autowired
    private RoleRepo roleRepo;

    //  Menampilkan data
    @GetMapping("/role")
    public ResponseEntity<List<Role>> GetAllPosition(){
        try {
            List<Role> position = this.roleRepo.findAll();
            return new ResponseEntity<>(position, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //  Open form & Save data
    @PostMapping("/role")
    public ResponseEntity<Object> SaveRole(@RequestBody Role role){
        Role roleData = this.roleRepo.save(role);
        if (roleData.equals(role)){
            return new ResponseEntity<>("success",HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>("faild",HttpStatus.BAD_REQUEST);
        }
    }

    //  mengedit data(Edit)
    @GetMapping("/role/{id}")
    public ResponseEntity<List<Role>> GetPositionById(@PathVariable("id") Long id) {
        try{
            Optional<Role> role = this.roleRepo.findById(id);
            if (role.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(role, HttpStatus.OK);
                return rest;
            } else{
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //  update data(Update)
    @PutMapping("/role/{id}")
    public ResponseEntity<Object> UpdateRole(@RequestBody Role role, @PathVariable("id") Long id)
    {
        Optional<Role> roleData = this.roleRepo.findById(id);

        if (roleData.isPresent())
        {
            role.setId(id);
            this.roleRepo.save(role);

            ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
            return rest;
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

    //  delete data(Delete)
    @DeleteMapping("/role/{id}")
    public ResponseEntity<Object> DeleteRole(@PathVariable("id")Long id)
    {
        this.roleRepo.deleteById(id);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }
}
