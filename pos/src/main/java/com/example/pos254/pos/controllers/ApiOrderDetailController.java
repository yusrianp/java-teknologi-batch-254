package com.example.pos254.pos.controllers;

import com.example.pos254.pos.models.OrderDetail;
import com.example.pos254.pos.repositories.OrderDetailRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiOrderDetailController {

    @Autowired
    private OrderDetailRepo orderDetailRepo ;

    @GetMapping("/orderdetail")
    public ResponseEntity<List<OrderDetail>> GetAllOrderDetail()
    {
        try {
            List<OrderDetail> orderDetail = this.orderDetailRepo.findAll();
            return new ResponseEntity<>(orderDetail, HttpStatus.OK);
        }catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/orderdetailbyorder/{id}")
    public ResponseEntity<List<OrderDetail>> getAllOrerById(@PathVariable("id") Long id)
    {
        try {
            List<OrderDetail> orderDetail = this.orderDetailRepo.FindByHeaderId(id);
            return new ResponseEntity<>(orderDetail,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/orderdetail")
    public ResponseEntity<Object> SaveOrderDetail(@RequestBody OrderDetail orderDetail)
    {
        OrderDetail orderDetailData = this.orderDetailRepo.save(orderDetail);
        if(orderDetailData.equals(orderDetail)){
            return new ResponseEntity<>("Save success", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("Save success", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/orderdetail/{id}")
    public ResponseEntity<Object> UpdateOrderDetail(@RequestBody OrderDetail orderdetail, @PathVariable("id") Long id)
    {
        Optional<OrderDetail> orderDetailData = this.orderDetailRepo.findById(id);
        if(orderDetailData.isPresent())
        {
            orderdetail.setId(id);
            this.orderDetailRepo.save(orderdetail);
            ResponseEntity rest = new ResponseEntity<>("Update success", HttpStatus.OK);
            return rest;
        }else {
            return ResponseEntity.notFound().build();
        }
    }
}
