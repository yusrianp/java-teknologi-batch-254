package com.example.pos254.pos.repositories;

import com.example.pos254.pos.models.Variant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VariantRepo extends JpaRepository<Variant, Long> {
    @Query("FROM Variant WHERE CategoryId = ?1")
    List<Variant> FindByCategoryId(Long categoryId);
}
