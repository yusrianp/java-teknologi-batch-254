package com.example.pos254.pos.repositories;

import com.example.pos254.pos.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface ProductRepo extends JpaRepository<Product, Long>{
    @Query("FROM Product WHERE VariantId = ?1")
    List<Product> FindByVarianId(Long variantId);
}
