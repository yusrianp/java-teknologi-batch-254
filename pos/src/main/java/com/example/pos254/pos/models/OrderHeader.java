package com.example.pos254.pos.models;

import javax.persistence.*;

@Entity
@Table(name="order_header")
public class OrderHeader {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long Id;

    @Column(name = "reference")
    private String Reference;

    @Column(name = "amount")
    private String Amount;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getReference() {
        return Reference;
    }

    public void setReference(String reference) {
        Reference = reference;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }
}
