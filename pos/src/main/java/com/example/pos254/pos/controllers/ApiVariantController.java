package com.example.pos254.pos.controllers;

import com.example.pos254.pos.models.Variant;
import com.example.pos254.pos.repositories.VariantRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiVariantController {

    @Autowired
    private VariantRepo variantRepo;

    @GetMapping("/variant")
    public ResponseEntity<List<Variant>> GetAllVariant()
    {
        try {
            List<Variant> variant = this.variantRepo.findAll();
            return new ResponseEntity<>(variant, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/variantbycategory/{id}")
    public ResponseEntity<List<Variant>> GetAllVariantByCategoryId(@PathVariable("id") Long id)
    {
        try
        {
            List<Variant> variant = this.variantRepo.FindByCategoryId(id);
            return new ResponseEntity<>(variant, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/variant")
    public ResponseEntity<Object> SaveVariant(@RequestBody Variant variant)
    {
        try
        {
            variant.setCreatedBy("indra");
            variant.setCreatedOn(new Date());
            this.variantRepo.save(variant);
            return new ResponseEntity<>(variant, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/variant/{id}")
    public ResponseEntity<List<Variant>> GetVariantById(@PathVariable("id") Long id)
    {
        try
        {
            Optional<Variant> variant = this.variantRepo.findById(id);

            if (variant.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(variant, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/variant/{id}")
    public ResponseEntity<Object> UpdateVariant(@RequestBody Variant variant, @PathVariable("id") Long id)
    {
        Optional<Variant> variantData = this.variantRepo.findById(id);

        if (variantData.isPresent())
        {
            variant.setId(id);
            this.variantRepo.save(variant);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/variant/{id}")
    public ResponseEntity<Object> DeleteVariant(@PathVariable("id") Long id)
    {
        this.variantRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
}
