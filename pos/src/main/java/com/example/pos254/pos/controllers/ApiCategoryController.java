package com.example.pos254.pos.controllers;

import com.example.pos254.pos.repositories.CategoryRepo;
import com.example.pos254.pos.models.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")

public class ApiCategoryController {

    @Autowired
    private CategoryRepo categoryRepo;

//  Menampilkan data
    @GetMapping("/category")
    public ResponseEntity<List<Category>> GetAllCategory(){
        try {
            List<Category> category = this.categoryRepo.findAll();
            return new ResponseEntity<>(category, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

//  Open form & Save data
    @PostMapping("/category")
    public ResponseEntity<Object> SaveCategory(@RequestBody Category category){
        try {
            category.setCreatedBy("indra");
            category.setCreatedOn(new Date());
            this.categoryRepo.save(category);
            return new ResponseEntity<>("success",HttpStatus.OK);
        }
        catch (Exception exception)
            {
            return new ResponseEntity<>("faild",HttpStatus.BAD_REQUEST);
        }
    }

//  mengedit data(Edit)
    @GetMapping("/category/{id}")
    public ResponseEntity<List<Category>> GetCategoryById(@PathVariable("id") Long id) {
        try{
            Optional<Category> category = this.categoryRepo.findById(id);
            if (category.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(category, HttpStatus.OK);
                return rest;
            } else{
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchcategory/{keyword}")
    public ResponseEntity<List<Category>> SearchCategoryName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Category> category = this.categoryRepo.SearchCategory(keyword);
            return new ResponseEntity<>(category, HttpStatus.OK);
        } else {
            List<Category> category = this.categoryRepo.findAll();
            return new ResponseEntity<>(category, HttpStatus.OK);
        }
    }

    @GetMapping("/categorymapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<Category> category = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Category> pageTuts;

            pageTuts = categoryRepo.findAll(pagingSort);

            category = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("category", category);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//  update data(Update)
    @PutMapping("/category/{id}")
    public ResponseEntity<Object> UpdateCategory(@RequestBody Category category, @PathVariable("id") Long id)
    {
        Optional<Category> categoryData = this.categoryRepo.findById(id);

        if (categoryData.isPresent())
        {
            category.setId(id);
            category.setModifiedBy("indra");
            category.setModifiedOn(new Date());
            this.categoryRepo.save(category);
            ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
            return rest;
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

//  delete data(Delete)
    @DeleteMapping("/category/{id}")
    public ResponseEntity<Object> DeleteCategory(@PathVariable("id")Long id)
    {
        this.categoryRepo.deleteById(id);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }

}
