package com.example.pos254.pos.models;

import javax.persistence.*;

@Entity
@Table(name = "variant")
public class Variant extends CommonEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long Id;

    @Column(name = "variant_code", nullable = false)
    private String VariantCode;

    @Column(name = "Variant_name", nullable = false)
    private String VariantName;

    @ManyToOne
    @JoinColumn(name = "category_id", insertable = false, updatable = false)
    public Category category;

    @Column(name = "category_id", nullable = true)
    private Long CategoryId;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getVariantCode() {
        return VariantCode;
    }

    public void setVariantCode(String variantCode) {
        VariantCode = variantCode;
    }

    public String getVariantName() {
        return VariantName;
    }

    public void setVariantName(String variantName) {
        VariantName = variantName;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Long getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(Long categoryId) {
        CategoryId = categoryId;
    }
}
