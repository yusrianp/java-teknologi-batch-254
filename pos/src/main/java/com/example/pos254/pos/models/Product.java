package com.example.pos254.pos.models;
import javax.persistence.*;

@Entity
@Table(name="Product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long Id;

    @ManyToOne
    @JoinColumn(name="variant_id", insertable = false, updatable=false)
    public Variant variant;

    @Column(name="variant_id")
    private Long VariantId;

    @Column(name="product_initial")
    private String ProductInitial;

    @Column(name="product_name")
    private String ProductName;

    @Column(name="product_description")
    private String ProductDescription;

    @Column(name="product_price")
    private Long ProductPrice;

    @Column(name="product_stock")
    private Long ProductStock;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Variant getVariant() {
        return variant;
    }

    public void setVariant(Variant variant) {
        this.variant = variant;
    }

    public Long getVariantId() {
        return VariantId;
    }

    public void setVariantId(Long variantId) {
        VariantId = variantId;
    }

    public String getProductInitial() {
        return ProductInitial;
    }

    public void setProductInitial(String productInitial) {
        ProductInitial = productInitial;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductDescription() {
        return ProductDescription;
    }

    public void setProductDescription(String productDescription) {
        ProductDescription = productDescription;
    }

    public Long getProductPrice() {
        return ProductPrice;
    }

    public void setProductPrice(Long productPrice) {
        ProductPrice = productPrice;
    }

    public Long getProductStock() {
        return ProductStock;
    }

    public void setProductStock(Long productStock) {
        ProductStock = productStock;
    }
}
