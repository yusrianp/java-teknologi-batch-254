package com.example.pos254.pos.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/orderdetail/")
public class OrderDetailController {
    @GetMapping(value="index")
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("orderdetail/index");
        return view;
    }
}
