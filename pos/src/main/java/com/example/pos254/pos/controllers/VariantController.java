package com.example.pos254.pos.controllers;

import com.example.pos254.pos.repositories.VariantRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/variant/")
public class VariantController {

    @Autowired
    private VariantRepo variantRepo;

    @GetMapping(value = "index")
    public ModelAndView index()
    {
        ModelAndView view = new ModelAndView("variant/index");
        return view;
    }

}
