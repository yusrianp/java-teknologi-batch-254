package com.example.pos254.pos.controllers;

import com.example.pos254.pos.models.Product;
import com.example.pos254.pos.repositories.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")

public class ApiProductController {
    @Autowired
    private ProductRepo productRepo;

    //  Menampilkan data
    @GetMapping("/product")
    public ResponseEntity<List<Product>> GetAllVarian(){
        try {
            List<Product> product = this.productRepo.findAll();
            return new ResponseEntity<>(product, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //  Mengambil id dari product
    @GetMapping("/productbyvarian/{id}")
    public ResponseEntity<List<Product>> GetAllProductByVarian(@PathVariable("id") Long id)
    {
        try{
            List<Product> product = this.productRepo.FindByVarianId(id);
            return new ResponseEntity<>(product,HttpStatus.OK);
        }
        catch(Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //  Save data
    @PostMapping("/product")
    public ResponseEntity<Object> SaveProduct(@RequestBody Product product){
        Product productData = this.productRepo.save(product);
        if (productData.equals(product)){
            return new ResponseEntity<>("success",HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>("faild",HttpStatus.BAD_REQUEST);
        }
    }

    //  mengedit data(Edit)
    @GetMapping("/product/{id}")
    public ResponseEntity<List<Product>> GetProductById(@PathVariable("id") Long id) {
        try{
            Optional<Product> product = this.productRepo.findById(id);
            if (product.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(product, HttpStatus.OK);
                return rest;
            } else{
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //  update data(Update)
    @PutMapping("/product/{id}")
    public ResponseEntity<Object> UpdateProduct(@RequestBody Product product, @PathVariable("id") Long id)
    {
        Optional<Product> productData = this.productRepo.findById(id);

        if (productData.isPresent())
        {
            product.setId(id);
            this.productRepo.save(product);

            ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
            return rest;
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

    //  delete data(Delete)
    @DeleteMapping("/product/{id}")
    public ResponseEntity<Object> DeleteProduct(@PathVariable("id")Long id)
    {
        this.productRepo.deleteById(id);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }
}
