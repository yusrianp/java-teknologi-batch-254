package com.example.pos254.pos.repositories;

import com.example.pos254.pos.models.OrderHeader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderHeaderRepo extends JpaRepository<OrderHeader, Long> {
    @Query("SELECT MAX(id) as maxId from OrderHeader")
    Long GetMaxOrderHeader();
}
