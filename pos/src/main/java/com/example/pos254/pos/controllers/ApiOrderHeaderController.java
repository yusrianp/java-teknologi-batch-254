package com.example.pos254.pos.controllers;

import com.example.pos254.pos.repositories.OrderHeaderRepo;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.pos254.pos.models.OrderHeader;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiOrderHeaderController {

    @Autowired
    private OrderHeaderRepo orderHeaderRepo;

    @GetMapping("/orderheader")
    public ResponseEntity<List<OrderHeader>> GetAllOrderHeader()
    {
        try{
            List<OrderHeader> orderHeader = this.orderHeaderRepo.findAll();
            return new ResponseEntity<>(orderHeader, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/orderheader/{id}")
    public ResponseEntity<List<OrderHeader>> getOrderHeaderById(@PathVariable("id") Long id){
        try{
            Optional<OrderHeader> orderHeader = this.orderHeaderRepo.findById(id);
            if(orderHeader.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(orderHeader, HttpStatus.OK);
                return rest;
            }else{
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/orderheadergetmaxid")
    public ResponseEntity<Long> GetOrderHeaderMaxId() {
        try {
            Long oh = this.orderHeaderRepo.GetMaxOrderHeader();
            System.out.println(oh);
            return new ResponseEntity<>(oh, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @PostMapping("/orderheader")
    public ResponseEntity<Object> SaveOrderHeader(@RequestBody OrderHeader orderHeader)
    {
        String reference = "" + System.currentTimeMillis();
        orderHeader.setReference(reference);
        OrderHeader orderHeaderData = this.orderHeaderRepo.save(orderHeader);
        if(orderHeaderData.equals(orderHeader)){
            return new ResponseEntity<>("Save Success", HttpStatus.OK);
        }else {
            return new ResponseEntity<>("Save Failed !!!", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/orderheader/{id}")
    public ResponseEntity<Object> UpdateOrderHeader(@RequestBody OrderHeader orderheader, @PathVariable("id") Long id)
    {
        Optional<OrderHeader> orderHeaderData = this.orderHeaderRepo.findById(id);
        if(orderHeaderData.isPresent())
        {
            orderheader.setId(id);
            this.orderHeaderRepo.save(orderheader);
            ResponseEntity rest = new ResponseEntity<>("Update sucess", HttpStatus.OK);
            return rest;
        }else {
            return ResponseEntity.notFound().build();
        }
    }


}
